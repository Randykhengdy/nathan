import com.TodoApp.TodoList;
import com.sun.tools.javac.comp.Todo;
import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {

    @Test
    public  void addTaskReturnFalseOnNull(){
        ToDoList todo = new ToDoList();
        Task task =null;
        boolean actual = todo.addTask(task);
        assertFalse(actual);

    }
    @Test
    public void addTaskReturnOnObject(){
        ToDoList todo = new ToDoList();
        Task task = new Task();

        boolean actual = todo.addTask(task);

        assertTrue(actual);
    }

    @Test
    public void getNumberOfTaskReturnZeroOnInitial(){
        ToDoList todo = new ToDoList();
        int expected =0;

        int actual = todo.getNumberOfTask();

        assertEquals(expected, actual);

    }

    @Test
    public void getNumberOfTaskReturnOneAfterAddTask(){
        ToDoList todo = new ToDoList();
        Task task = new Task();
        int expected =1;

        todo.addTask(task);
        int actual = todo.getNumberOfTask();

        assertEquals(expected, actual);

    }
}